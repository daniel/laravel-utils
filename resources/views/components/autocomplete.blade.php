@props(['url', 'id'])
@php
$id = $id ?? rand(0, 99999);
@endphp
<div x-data="initAutocomplete_{{$id}}()">

    <div class="relative">
        <input autofocus="autofocus" class="border border-gray-100 p-2 rounded-xl w-full bg-gray-50 focus:bg-white"
            name="q" x-on:input.debounce.500="getData(q)" x-model="q" />
        <div x-show="rows.length > 0"
            class="z-50 absolute mt-1 bg-white shadow w-full border border-gray-100 rounded-sm">

            {{$slot}}


        </div>
    </div>

    <script type="text/javascript">
        function initAutocomplete_{{$id}}() {
            var url = '{{$url ?? null}}';
            return {
                q: null,
                rows: [],
                getData(q) {
                    this.rows = [];
                    if (q.length < 3) {
                        return;
                    }
                    
                    let rows = this.rows;
                    fetch(url + q)
                    .then(response => response.json())
                    .then(data => {
                        this.rows = data
                    })    
                },
                selectRow(cb) {
                    cb && cb();
                    this.rows = [];
                    this.q = null;
                }
            }
        } 
        
    </script>
</div>