@php
$color = $color ?? 'blue';
@endphp
<div class="md:mr-10 w-full mb-3 sm:w-3/4 md:w-80 h-40 shadow-sm border bg-{{$color}}-400 rounded-lg  transitions-all hover:border-{{$color}}-200 hover:shadow-lg px-3">
    <a class="w-full h-full flex flex-row items-center justify-between" href="{{$href ?? null}}">
        <div class="flex flex-row items-baseline">
            <i class="text-4xl text-white fa fa-{{$icon ?? 'home'}} mb-2"></i>
            <div class="text-white text-center text-4xl ml-5">{{__($label) ?? 'sample label'}}</div>
        </div>
        <i class="text-4xl fas text-white fa-chevron-right"></i>
    </a>
</div>