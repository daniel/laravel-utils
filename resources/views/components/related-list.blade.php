@props(['updateURL', 'deleteURL', 'addURL', 'onAdd', 'id'])
@php
$id = $id ?? rand(0, 99999);
@endphp
<div x-data="initUsers_{{$id}}()" x-init="update_{{$id}}()" x-on:{{$onAdd ?? 'addobj'
    }}.window="addObj($event.detail.id)">
    <div class="mt-2">
        {{$slot}}
    </div>
    <script type="text/javascript">
        function initUsers_{{$id}}() {
            var updateURL = '{{$updateURL ?? null}}';
            var deleteURL = '{{$deleteURL ?? null}}';
            var addURL = '{{$addURL ?? null}}';
            
            console.log('{{$id}} initialized');
            console.log(updateURL);

            return {
                rows: [],
                update_{{$id}}() {
                    fetch(updateURL)
                    .then(response => response.json())
                    .then(data => {
                        this.rows = data
                    })
                },
                deleteObj(userId) {
                    
                    fetch(deleteURL+userId, {
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token()}}'
                        }
                    })
                    .then(response => response.json())
                    .then(data => {
                        //console.log(data);
                        this.update_{{$id}}();
                    })
                },
                addObj(userId) {
                    //console.log(JSON.stringify(user));
                    fetch(addURL, {
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token()}}'
                        },
                        body: JSON.stringify(userId)
                    })
                    .then(response => response.json())
                    .then(data => {
                        //console.log(data);
                        
                        this.update_{{$id}}();
                    })
                }
            }
        }
    </script>
</div>