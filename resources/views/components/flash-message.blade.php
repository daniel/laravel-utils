@if ($message = Session::get('success'))
    <x-teix-utils::alert
        color="green"
        :message="$message"
    ></x-teix-utils::alert>
@endif

@if ($message = Session::get('error'))
    <x-teix-utils::alert
        color="red"
        :message="$message"
    ></x-teix-utils::alert>
@endif

@if ($message = Session::get('warning'))
    <x-teix-utils::alert
        color="pink"
        :message="$message"
    ></x-teix-utils::alert>
@endif

@if ($message = Session::get('info'))
    <x-teix-utils::alert
        color="blue"
        :message="$message"
    ></x-teix-utils::alert>
@endif

@if ($errors->any())
    <x-teix-utils::alert
        color="red"
        message="Ha habido algunos errores"
    >
        <div>
            <ul class="list-disc">
                @foreach ($errors->all() as $error)
                    <li class="ml-5">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </x-teix-utils::alert>
@endif
