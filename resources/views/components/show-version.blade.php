@php

try {
    $version = file_get_contents(base_path('commit-version.txt'));
} catch (\Exception $e) {
    $version = 'version file missing';
}
@endphp

<div>
    ver: {{ $version }}
</div>
