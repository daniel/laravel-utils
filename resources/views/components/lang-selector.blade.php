@if (config('app.locales') && count(config('app.locales'))>1)


<div class="flex flex-row items-center justify-center">
    <i class="fa fa-language mr-1"></i>
    @php
    $currentLocale = App::getLocale();

    @endphp
    @foreach (config('app.locales') as $locale)
    @if ($locale == $currentLocale)
    <span class="bg-green-100 rounded-lg p-1 mr-3">{{$locale}}</span>
    @else
    <a class="mr-3 bg-gray-50 rounded-lg p-1 " href="{{route('setLocale', $locale)}}">{{$locale}}</a>
    @endif

    @endforeach
</div>

@endif