@props(['active', 'icon', 'disabled', 'id'])

@php
$defClasses = 'font-medium leading-5 ';
$color = 'gray';
$baseTint = $disabled ?? false ? 300 : 500;
$classes =
    $active ?? false
        ? "bg-$color-200 border-$color-400 text-$color-900
focus:outline-none transition duration-150 ease-in-out"
        : " border-b-2 border-transparent text-{$color}-$baseTint
hover:text-$color-700 hover:bg-$color-100 transition duration-150 ease-in-out ";
$classes = $defClasses . $classes;
@endphp
<li
    id="{{ $id ?? '' }}"
    class="w-full pl-4 pr-4 flex-row flex items-center   text-{{ $color }}-{{ $baseTint }} py-2 {{ $classes }}"
>
    <i class="fa fa-{{ $icon }} w-auto mr-2 "></i>
    <a {{ $attributes->merge(['class' => $classes]) }}>
        {{ $slot }}
    </a>
    <i class="fa fa-chevron-right ml-auto "></i>
</li>
