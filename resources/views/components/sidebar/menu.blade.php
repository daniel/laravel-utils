@if (auth()->user()->hasRole('admin'))
    <ul
        id="admin-menu"
        class="mb-4 border-b-2 pb-4"
    >
        {{-- <x-sidebar.item icon="columns" :active="request()->routeIs('dashboard.admin')"
        :href="route('dashboard.admin')">
        {{__('Dashboard')}}
    </x-sidebar.item>
    <x-sidebar.item icon="users" :active="request()->routeIs('users.index')" :href="route('users.index')">
        {{__('Users')}}
    </x-sidebar.item>
    <x-sidebar.item :active="request()->routeIs('groups.index')" :href="route('groups.index')" icon="star">
        {{__('Groups')}}</x-sidebar.item> --}}

        <x-teix-utils::sidebar.item
            icon="users"
            :active="request()->routeIs('users.index')"
            :href="route('users.index')"
        >
            {{ __('Users') }}
            </x-sidebar.item>


    </ul>
@endif

<ul
    id="user-menu"
    class="mb-4"
>



</ul>
