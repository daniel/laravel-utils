@php
$defaultAttributes = [
'type' => 'text'
];
$attributes = array_merge($defaultAttributes, $attributes);
$label = $attributes['label'] ?? $name;
@endphp
<div class="mb-4 sm:flex flex-row">
    @if ($label !== false && $attributes['type'] != 'checkbox')
    <?= Form::label($name, $label ?? $name, ['class'=>'font-bold text-sm block w-full sm:w-1/4 ']) ?>
    @endif

    @if ($attributes['type'] == 'text')
    <?= Form::text($name, $value, array_merge($attributes, ['class'=>'w-full rounded-lg border-gray-200'])) ?>
    @endif
    @if ($attributes['type'] == 'hidden')
    <?= Form::hidden($name, $value, array_merge($attributes, [])) ?>
    @endif
    @if ($attributes['type'] == 'email')
    <?= Form::email($name, $value, ['class'=>'w-full rounded-lg border-gray-200']) ?>
    @endif
    @if ($attributes['type'] == 'file')
    <?= Form::file($name, $value, ['class'=>'w-full rounded-lg border-gray-200']) ?>
    @endif
    @if ($attributes['type'] == 'number')
    <?= Form::number($name, $value, ['class'=>'w-full rounded-lg border-gray-200']) ?>
    @endif
    @if ($attributes['type'] == 'textarea')
    <?= Form::textarea($name, $value, ['class'=>'w-full rounded-lg border-gray-200']) ?>
    @endif
    @if ($attributes['type'] == 'password')
    <?= Form::password($name, null, ['class'=>'w-full rounded-lg border-gray-200']) ?>
    @endif
    @if ($attributes['type'] == 'date')
    <?= Form::date($name, $value, ['class'=>'w-full rounded-lg border-gray-200']) ?>
    @endif
    @if ($attributes['type'] == 'datetime-local')
    <?= Form::input('datetime-local', $name, $value ? $value->format('Y-m-d\TH:i') : null, ['class'=>'w-full rounded-lg border-gray-200', 'step'=>15]) ?>
    @endif
    @if ($attributes['type'] == 'checkbox')
    {{-- //empty label for formatting --}}
    <?= Form::label($name, ' ', ['class'=>'font-bold text-sm block w-full sm:w-1/5 ']) ?>
    <?= Form::checkbox($name, 1, $value , ['class'=>'mr-2']) ?>
    @if ($label !== false)
    <?= Form::label($name, $label ?? $name, ['class'=>'font-bold text-sm']) ?>
    @endif
    @endif
    @if ($attributes['type'] == 'select')
    @php
    $data = $attributes['data'];
    unset($attributes['data']);
    @endphp
    <?= Form::select($name, $data, $value , array_merge($attributes, ['class'=>'w-full rounded-lg border-gray-200'])) ?>
    @endif
</div>