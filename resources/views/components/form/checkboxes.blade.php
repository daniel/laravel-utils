@php

$name = @$attributes['name'] ?? "name";
$id = @$attributes['id'] ?? 'id';
$fieldName = @$attributes['field'];

$inline= @$attributes['inline'] ? 'flex' : '';
@endphp
<ul class="<?= $inline ?>">
    @foreach ($data as $item)
    @php
    $fieldId = $fieldName."_".$item->id; //TODO not working o
    @endphp

    <li>

        <?= Form::checkbox($fieldName.'[]', $item->$id, null, ['id'=>$fieldId]) ?>
        <label for="<?=$fieldId ?>" class=" mr-2"><?=$item->name ?></label>
    <li>
        @endforeach
</ul>