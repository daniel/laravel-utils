@props(['fieldName', 'uploadLabel', 'color', 'textColor', 'viewColor', 'callback', 'autoSubmit', 'item'])
@php
$fieldName = $fieldName ?? 'media_file';
//$label = $label ?? trans('Adjuntar');
$uploadLabel = $uploadLabel ?? null;
$item = $item ?? null;
$mediaFile = $mediaFile ?? null;
$viewColor = $viewColor ?? 'indigo';
$color = $color ?? 'blue';
$textColor = $textColor ?? 'text-white';
$callback = $callback ?? '';
$autoSubmit = $autoSubmit ?? false;

@endphp

<div class="pb-1 border-b-2 border-gray-100 mb-4 flex flex-row justify-start">
    @if ($item)
        <div class="w-1/2 mr-auto ">
            <p class="font-bold text-sm block">{{ $item->file_name }}</p>
            <div class="flex flex-row justify-start">
                <div class="w-3/5 text-sm">
                    <p class="mb-3 text-sm">{{ trans('Fecha de subida') }}:
                        {{ $item->updated_at->format(config('app.dateTimeFormat')) }}</p>
                </div>
            </div>
        </div>
        @if (auth()->user()->hasRole('admin'))


            <div class=" ">
                <x-form.action-button
                    class="fa-lock mr-2"
                    color="{{ $item->hasCustomProperty('disabled') ? 'red' : 'gray' }}"
                    href="{{ route('files.toggleEditable', $item->id) }}"
                />


            </div>
        @endif
        <div class=" ">
            <x-form.action-button
                class="fa-eye mr-2"
                color="{{ $viewColor }}"
                href="{{ $item->getFullUrl() }}"
                target="_blank"
            />


        </div>
        @if (auth()->user()->hasRole('admin') ||
    !$item->hasCustomProperty('disabled'))


            <div class="flex flex-col">
                <div class="flex flex-row justify-center items-center">
                    <label
                        for="media_id"
                        onclick="displayUploadedFileName(this, undefined,  {{ $autoSubmit ? 'true' : 'false' }})"
                        class=""
                    >
                        <x-form.action-button
                            class="fa-trash mr-2"
                            color="red"
                        />
                    </label>
                </div>
                <div class="uploadDisplay text-xs text-gray-400"></div>
            </div>
        @endif
    @endif

    <div class="flex flex-col uploadElement">
        <div class="flex flex-row">
            <label
                for="{{ $fieldName }}"
                class=""
            >
                <x-form.action-button
                    class="fa-file-upload mr-2"
                    color="blue"
                    label="{{ $uploadLabel }}"
                />
            </label>

            <input
                onchange="displayUploadedFileName(this, undefined,  {{ $autoSubmit ? 'true' : 'false' }})"
                class="hidden"
                name="{{ $fieldName }}"
                type="file"
                id="{{ $fieldName }}"
            />

            <input
                type="hidden"
                name="media_id"
                value="{{ $item->id ?? -1 }}"
            />
        </div>
        <div class="uploadDisplay text-xs text-gray-400"></div>
    </div>

</div>
<script>
    function displayUploadedFileName(inputElement, cb = undefined, autoSubmit = false) {
        console.log(inputElement);
        cb && console.log(cb);
        let parent = inputElement.closest('div[class="flex flex-col uploadElement"]');
        try {
            if (!autoSubmit) {
                let fullPath = inputElement.value;
                let displayElement = parent?.getElementsByClassName('uploadDisplay')[0];
                displayElement.innerHTML = inputElement?.files[0]?.name;
            }
        } catch (e) {

        }

        try {


            let icon = parent.getElementsByClassName('fa')[0];
            icon.classList.forEach((c, i) => {
                c.substr(0, 3) === 'fa-' && icon.classList.remove(c);
            })
            icon.classList.add('fa-spinner', 'fa-pulse');
        } catch (e) {
            //
        }

        cb && cb(inputElement);

        if (autoSubmit) {
            inputElement.closest('form')?.submit();
        }
    }
</script>
