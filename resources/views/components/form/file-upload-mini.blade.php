@props(['fieldName', 'uploadLabel', 'color', 'textColor', 'viewColor', 'callback', 'autoSubmit', 'item'])
@php
$fieldName = $fieldName ?? 'media_file';
//$label = $label ?? trans('Adjuntar');
$uploadLabel = $uploadLabel ?? null;
$item = $item ?? null;
$mediaFile = $mediaFile ?? null;
$viewColor = $viewColor ?? 'indigo';
$color = $color ?? 'blue';
$textColor = $textColor ?? 'text-white';
$callback = $callback ?? '';
$autoSubmit = $autoSubmit ?? false;

@endphp

<div class="">


    <div class="flex flex-col uploadElement">
        <div class="flex flex-row">
            <label for="{{$fieldName}}" class="">
                <x-form.action-button class="fa-upload mr-2" color="blue" label="{{$uploadLabel}}" /> </label>

            <input onchange="displayUploadedFileName(this, undefined,  {{$autoSubmit ? 'true' : 'false'}})"
                class="hidden" name="{{$fieldName}}" type="file" id="{{$fieldName}}" />

            <input type="hidden" name="media_id" value="{{$item->id ?? -1}}" />
        </div>
        <div class="uploadDisplay text-xs text-gray-400"></div>
    </div>

</div>
<script>
    function displayUploadedFileName(inputElement, cb=undefined, autoSubmit=false) { 
        console.log(inputElement);
        let parent = inputElement.closest('div[class="flex flex-col uploadElement"]');   
        try {
            if (!autoSubmit) {
                let fullPath = inputElement.value;
                let displayElement = parent?.getElementsByClassName('uploadDisplay')[0];
                displayElement.innerHTML = inputElement?.files[0]?.name;
            }
        } catch (e) {   

        }

        try {

        
            let icon = parent.getElementsByClassName('fa')[0];
            icon.classList.forEach((c, i) => {
                c.substr(0,3) === 'fa-' && icon.classList.remove(c);
            })
            icon.classList.add('fa-spinner', 'fa-pulse');
        } catch (e) {
            //
        }
         
        cb && cb(inputElement);
        
        if (autoSubmit) {           
            inputElement.closest('form')?.submit();
        }
    }
</script>