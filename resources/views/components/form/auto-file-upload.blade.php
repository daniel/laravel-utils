@props(['route', 'model', 'item'])
@php
$item = $item ?? null;
@endphp

{{Form::model($model, ['route' => $route, 'files'=>true])}}
@method('POST')

<x-form.file-upload :autoSubmit="true" {{$attributes->merge(['item'=>$item, ])}} viewColor="yellow" label="" />
{{Form::close()}}