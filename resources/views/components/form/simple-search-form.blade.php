<form action="" method="get" class="flex space-x-1 flex-row items-center mb-2">
    <input id="q" class="border border-gray-300 rounded-lg p-1 w-full" name="q" value="{{$q ?? ''}}" />
    <x-button color="blue">{{__('Search')}}</x-button>
</form>