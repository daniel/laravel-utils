@php
$color = $color ?? 'gray';
$iconClass = " fa fa-".($icon ?? 'home');
$type = $type ?? 'link';

@endphp
{{--
@if ($attributes['type']=='form')
<form method="POST" action="{{ $href }}" onclick="event.preventDefault(); confirm('Are you sure?') && this.submit();">
@csrf
@method('DELETE')
@endif

<div class=" bg-{{$color}}-600 p-1 rounded-lg  {{$containerClass ?? ''}} m-1" ">
        <a class=" flex-row flex" href="{{$href ?? ''}}">
    <i class="{{$class}}"></i>
    @if (isset($label))
    <span class="inline text-xs font-normal ml-1 text-white">{{$label ?? ''}}</span>
    @endif
    </a>
</div>
@if ($attributes['type']=='form')
</form>
@endif --}}
@if ($attributes['type']=='form')
<form class="inline" method="POST" action="{{ $href }}"
    onclick="event.preventDefault(); <?= "confirm('".trans('¿Estás seguro?')."')" ?> && this.submit();">
    @csrf
    @method('DELETE')
    @endif
    <span>
        <a
            {{ $attributes->merge(['class'=>" fa rounded-full p-2 my-1 bg-$color-500 text-white transition-colors duration-200 transform hover:bg-$color-800" ]) }}>

            @if (isset($label))
            <span class="text-xs font-light">
                {{$label}}
            </span>

            @endif
        </a>
    </span>
    @if ($attributes['type']=='form')
</form>
@endif