# Teix Laravel Utils

[![Latest Version on Packagist](https://img.shields.io/packagist/v/teix/laravel-utils.svg?style=flat-square)](https://packagist.org/packages/teix/laravel-utils)
[![Total Downloads](https://img.shields.io/packagist/dt/teix/laravel-utils.svg?style=flat-square)](https://packagist.org/packages/teix/laravel-utils)


Various utilities for laravel projects

## Installation

You can install the package via composer:

```bash
composer require teix/laravel-utils
```

## Usage

```php
// Usage description here
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email dan@teixweb.com instead of using the issue tracker.

## Credits

-   [Daniel Z](https://github.com/teix)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

